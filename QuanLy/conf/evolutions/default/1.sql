# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table address (
  id                        bigint not null,
  street                    varchar(255),
  number                    varchar(255),
  postal_code               varchar(255),
  city                      varchar(255),
  country                   varchar(255),
  constraint pk_address primary key (id))
;

create table quanly (
  id                        bigint not null,
  name                      varchar(255),
  address_id                bigint,
  constraint pk_quanly primary key (id))
;

create table stock_item (
  id                        bigint not null,
  quanly_id                 bigint,
  student_id                bigint,
  quantity                  bigint,
  constraint pk_stock_item primary key (id))
;

create table student (
  id                        bigint not null,
  mssv                      varchar(255),
  name                      varchar(255),
  email                     varchar(255),
  date                      timestamp,
  faculty                   varchar(255),
  phone                     varchar(255),
  address                   varchar(255),
  picture                   bytea,
  constraint pk_student primary key (id))
;

create table tag (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_tag primary key (id))
;

create table user_account (
  id                        bigint not null,
  email                     varchar(255),
  password                  varchar(255),
  constraint pk_user_account primary key (id))
;


create table student_tag (
  student_id                     bigint not null,
  tag_id                         bigint not null,
  constraint pk_student_tag primary key (student_id, tag_id))
;
create sequence address_seq;

create sequence quanly_seq;

create sequence stock_item_seq;

create sequence student_seq;

create sequence tag_seq;

create sequence user_account_seq;

alter table quanly add constraint fk_quanly_address_1 foreign key (address_id) references address (id);
create index ix_quanly_address_1 on quanly (address_id);
alter table stock_item add constraint fk_stock_item_quanly_2 foreign key (quanly_id) references quanly (id);
create index ix_stock_item_quanly_2 on stock_item (quanly_id);
alter table stock_item add constraint fk_stock_item_student_3 foreign key (student_id) references student (id);
create index ix_stock_item_student_3 on stock_item (student_id);



alter table student_tag add constraint fk_student_tag_student_01 foreign key (student_id) references student (id);

alter table student_tag add constraint fk_student_tag_tag_02 foreign key (tag_id) references tag (id);

# --- !Downs

drop table if exists address cascade;

drop table if exists quanly cascade;

drop table if exists stock_item cascade;

drop table if exists student cascade;

drop table if exists student_tag cascade;

drop table if exists tag cascade;

drop table if exists user_account cascade;

drop sequence if exists address_seq;

drop sequence if exists quanly_seq;

drop sequence if exists stock_item_seq;

drop sequence if exists student_seq;

drop sequence if exists tag_seq;

drop sequence if exists user_account_seq;

