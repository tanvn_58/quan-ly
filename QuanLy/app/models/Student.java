package models;
 
import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import java.util.*;
import play.mvc.PathBindable;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class Student extends Model implements PathBindable<Student> {
	
	@Id
    public Long id;

  @Constraints.Required
  public String mssv;
  @Constraints.Required
  public String name;
  @Constraints.Required
  public String email;
  public Date date;
  @Constraints.Required
  public String faculty;
  public String phone;
  public String address;

  @OneToMany(mappedBy="student")
  public List<StockItem> stockItems;
 
    public byte[] picture;
 
  public Student() {}
  public Student(String mssv, String name, String email, String faculty) {
    this.mssv = mssv;
    this.name = name;
    this.email = email;
	this.faculty = faculty;
  }
  public String toString() {
    return String.format("%s - %s", mssv, name);
  }
	
	
  private static List<Student> students;
  static {
    students = new ArrayList<Student>();
    students.add(new Student("13020372", "Vũ Ngọc Tân",
        "tanvn_58@vnu.edu.vn", "Công nghệ Thông tin"));
  }
  
  public static Finder<Long,Student> find = new Finder<Long,Student>(Long.class, Student.class);
  public static List<Student> findAll() {
    return find.all();
  }
 
  public static Student findByEan(String mssv) {
	return find.where().eq("mssv", mssv).findUnique();
  }
 
    public static Student findByEmail(String email) {
        return find.where().eq("email", email).findUnique();
    }
 
  public static List<Student> findByName(String term) {
    final List<Student> results = new ArrayList<Student>();
    for (Student candidate : students) {
      if (candidate.name.toLowerCase().contains(term.toLowerCase())) {
        results.add(candidate);
      }
    }
    return results;
  }
 
  
  @ManyToMany
  public List<Tag> tags = new LinkedList<Tag>();
  
    @Override
    public Student bind(String key, String value) {
        return findByEan(value);
    }
 
    @Override
    public String unbind(String key) {
        return mssv;
    }
 
    @Override
    public String javascriptUnbind() {
        return mssv;
    }
	public void delete() {
		for (Tag tag : tags) {
            tag.students.remove(this);
            tag.save();
        }
        for (StockItem stockItem : stockItems) {
            stockItem.delete();
        }
        super.delete();
		
    }
	public static Page<Student> find(int page) {  // trả về trang thay vì List
		return find.where()
              .orderBy("mssv asc")     // sắp xếp tăng dần theo id
              .findPagingList(5)    // quy định kích thước của trang
              .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
              .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
	}
}