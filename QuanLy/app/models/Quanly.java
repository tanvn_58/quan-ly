package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.*;

@Entity
public class Quanly extends Model{
    @Id
    public Long id;
	
    public String name;
	
    @OneToOne
    public Address address;
	
    @OneToMany(mappedBy = "quanly")
    public List<StockItem> stock = new ArrayList(); 
 
    public String toString() {
        return name;
    }
	public static Finder<Long, Quanly> find =
               new Finder<>(Long.class, Quanly.class);
}