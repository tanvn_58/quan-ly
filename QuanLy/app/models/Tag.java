package models;
 
import play.db.ebean.Model;
import play.data.validation.Constraints;
import java.util.*;
import javax.persistence.*;

@Entity
public class Tag extends Model {
	@Id
    public Long id;
    @Constraints.Required
    public String name;
	
    @ManyToMany(mappedBy="tags")
    public List<Student> students;
 
    public Tag(){
        // Left empty
    }
    public Tag(Long id, String name, Collection<Student> students) {
        this.id = id;
        this.name = name;
        this.students = new LinkedList<Student>(students);
        for (Student student : students) {
            student.tags.add(this);
        }
    }
	
	
/*	private static List<Tag> tags = new LinkedList<Tag>();*/
 
/*    static {
        tags.add(new Tag(1L, "Nam",
            Student.findByName("paperclips")));
        tags.add(new Tag(2L, "Nữ",
            Student.findByName("paperclips")));
    }*/
	
	public static Finder<Long, Tag> find =
               new Finder<>(Long.class, Tag.class);
			   
    public static Tag findById(Long id) {
        return find.byId(id);
    }
}