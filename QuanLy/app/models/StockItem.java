package models;

import play.db.ebean.Model;
import javax.persistence.*;


@Entity
public class StockItem extends Model{
    @Id
    public Long id;
 
    @ManyToOne
    public Quanly quanly;
 
    @ManyToOne
    public Student student;
 
    public Long quantity;
 
    public String toString() {
		return String.format("StockItem %d - %d x student %s",
			id, quantity, student == null ? null : student.id);
    }
	public static Finder<Long, StockItem> find =
               new Finder<>(Long.class, StockItem.class);
}