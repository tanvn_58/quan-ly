package controllers;

import com.avaje.ebean.Page;
import models.*;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.students.*;
import java.util.ArrayList;
import java.util.List;
import play.data.Form;
import com.avaje.ebean.Ebean;
import play.mvc.Security;

@Security.Authenticated(Secured.class)
public class Students extends Controller {
	private static final Form<Student> studentForm = Form.form(Student.class);
	
	public static Result list(Integer page) {
		/*UserAccount useracc = UserAccount.findByEmail(session().get("email"));
        if (useracc == null){
            return redirect(routes.Students.detailUser(Student.findByEmail(session().get("email"))));
        }*/
		Page<Student> students = Student.find(page);
		return ok(views.html.catalog.render(students));
	}
	
	public static Result newStudent(){
		return ok(details.render(studentForm));
	}
	
	public static Result save(){
		  Form<Student> boundForm = studentForm.bindFromRequest();
		  	
		  
		  if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		  }
		  Student student = boundForm.get();

/*		    if(!boundForm.field("password").valueOr("").isEmpty()){
            if(!boundForm.field("password").valueOr("").equals(boundForm.field("confirm_password").value())){
                boundForm.reject("confirm_password","Password don't match");
            }}*/
			/*if(!boundForm.field("email").valueOr("").isEmpty()){
            String tempMail = boundForm.field("email").value();
            if(Student.findByEmail(tempMail) != null){
                boundForm.reject("email", "Email đã tồn tại, yêu cầu nhập lại email");
            }}*/
			
			
			List<Tag> tags = new ArrayList<Tag>();
			for (Tag tag : student.tags) {
			  if (tag.id != null) {
				tags.add(Tag.findById(tag.id));
			  }
			}
			student.tags = tags;
			

			if (student.id == null) {
				if(Student.findByEan(student.mssv) == null) {
					student.save();
				}
			} else {
				student.update();
			}
			
			StockItem stockItem = new StockItem();
			stockItem.student = student;
			stockItem.quantity = 0L;
			stockItem.save();
			
		  flash("success", String.format("Successfully added student %s", student));
		  return redirect(routes.Students.list(0));		
	}
	
    public static Result details(Student student) {
        if (student == null) {
            return notFound(String.format("student %s does not exist.",
                            student.mssv));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));
    }
	

	
	public static Result delete(String mssv) {
	   final Student student = Student.findByEan(mssv);
	   if(student == null) {
		  return notFound(String.format("Student %s does not exists.", mssv));
	   }
		  			
	   /*for (StockItem stockItem : student.stockItems) {
			stockItem.delete();}*/
	   student.delete();	   
	   return redirect(routes.Students.list(0));
	}

 
}